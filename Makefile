GO ?= go

# build and run on the local OS.
GO_BUILD = go build

# build a "scratch" image friendly linux binary
GO_DIST = CGO_ENABLED=0 GOOS=linux GOARCH=amd64 $(GO_BUILD) -a -tags netgo -ldflags '-w'

.PHONY: dist

all: clean build

deps:
	go get -t ./...

dist: # test
	mkdir -p dist
	$(GO_DIST) -o dist/goose cmd/goose/*.go

test:
	$(GO) test

clean:
	rm -rf build dist
